"""
HTTPDownloader
"""

from __future__ import print_function

import os
import requests

from FileGetter import FileGetter


class HTTPDownloader(FileGetter):
    """
    Download a specified file outputing progress information if stdout is to a
    terminal.
    """

    def get_file(self, prefix, url):
        """
        Downloads a file from the provided URL and returns the data to the
        calling function.

        Args:
            prefix: The string to prefix the progress display with.
            url: The remote filename to download

        Returns:
            The data down loaded from the URL

        """
        logger = self._class_logger.getChild("get_file")

        data = ""

        logger.debug("Downloading {} using proxies {}".format(
            url, self._proxies))
        logger.debug("Prefix is {}".format(prefix))
        request = requests.get(url, proxies=self._proxies, stream=True)
        if request.status_code != 200:
            request.raise_for_status()

        tot = float(request.headers['content-length'])
        prog = 0

        for chunk in request.iter_content(chunk_size=65536):
            prog = prog + len(chunk)
            self._print_func("{} {}".format(prefix, os.path.basename(url)),
                             (prog/tot))
            data = data + chunk
        print()
        return data
