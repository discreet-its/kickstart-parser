"""
RepoParser
"""

from __future__ import print_function

import os
import bz2
import sqlite3
import pprint

from tempfile import NamedTemporaryFile
from io import StringIO
from defusedxml.ElementTree import iterparse, parse as xmlparse

from HTTPDownloader import HTTPDownloader
from FileSystemRetriever import FileSystemRetriever
from UtilFunctions import unique


class RepoParser(object):
    """
    Provides functionality to parse/query yum "repomd" files including the
    sqlite databases and the comps.xml groups file.
    """

    def get_repomd(self):
        """
        Gets the repomd.xml file from the repo and stores its content
        internally

        Returns:
            Nothing

        """
        logger = self._class_logger.getChild("get_repomd")
        logger.debug("Downloading repomd.xml")
        self._repomd = "%s" % \
            self._downloader.get_file("", self._base_url +
                                      "/repodata/repomd.xml")
        logger.debug("self._repomd is now {} bytes".format(len(self._repomd)))

    def query_repomd(self, data_type):
        """
        Queries the internal repomd xml for the URL relative to the base for
        the file providing the data of the requested type.

        Example:
            query_repomd("primary_db") should return something similar to
            "repodata/cb600e2b..73336623-primary.sqlite.bz2"

            query_repomd("") should return something similar to
            "repodata/cb600e2b..73336623-primary.sqlite.bz2"

        Args:
            data_type: The repo data type to obtain

        Returns:
            The relative URL to the file containing data of the type requested

        """
        logger = self._class_logger.getChild("query_repomd")

        if self._repomd is None:
            logger.debug("self._repomd is None")
            self.get_repomd()

        # Parse the XML
        _iterator = iterparse(StringIO(u"{}".format(self._repomd)))
        for _, _element in _iterator:
            if '}' in _element.tag:
                # Strip all name spaces
                _element.tag = _element.tag.split('}', 1)[1]

        logger.debug(
            "Attempting to query repomd for type {}".format(data_type))
        location = _iterator.root.find(
            "data[@type='%s']/location" % data_type).get("href")
        logger.debug(
            "repo data for type {} is at {}".format(data_type, location))
        return location

    def find_primary_db(self):
        """
        Calls query_repomd for the URL where the "primary_db" can be found.

        Returns:
            An absolute URL consisting of the base URL and the relative URL
            obtained from the query_repomd call.

        """
        logger = self._class_logger.getChild("find_primary_db")
        logger.debug("Finding repo primary database")
        primary_db = self._base_url + self.query_repomd("primary_db")
        logger.debug("primary_db is at {}".format(primary_db))
        return primary_db

    def find_comps_xml(self):
        """
        Calls query_repomd for the URL where the "group" file can be found.

        Returns:
            An absolute URL consisting of the base URL and the relative URL
            obtained from the query_repomd call.

        """
        logger = self._class_logger.getChild("find_comps_xml")
        logger.debug("Finding repo comps xml file")
        comps_xml = self._base_url + self.query_repomd("group")
        logger.debug("comps.xml is at {}".format(comps_xml))
        return comps_xml

    def download_comps_xml(self):
        """
        Using the previously instantiated HTTPDownloader class, downloads the
        groups XML file and returns it to the calling function.

        Returns:
            The content of the comps.xml file

        """
        logger = self._class_logger.getChild("download_comps_xml")
        logger.debug("Downloading comps XML")
        comps_xml_data = self._downloader.get_file("", self.find_comps_xml())
        logger.debug(
            "comps_xml_data has length {}".format(len(comps_xml_data)))
        return comps_xml_data

    def parse_comps_xml(self):
        """
        Parses the content of the comps XML file.

        Returns:
            A dictionary keyed by the @group name with the value being a list
            of packages indicated as neither optional or conditional
            (basically meaning mandatory or default) for each group.

        """
        logger = self._class_logger.getChild("parse_comps_xml")
        logger.debug("Parsing comps XML data")
        groups = {}

        root = xmlparse(StringIO(self._comps_xml)).getroot()
        for group in root.findall('group'):
            group_id = "@%s" % group.find('id').text
            groups[group_id] = []
            for package in group.iter('packagereq'):
                if package.attrib['type'] != 'optional' and \
                        package.attrib['type'] != 'conditional':
                    groups[group_id].append(package.text)
        return groups

    def get_packages_for_group(self, group):
        """
        Returns the list of non-optional and non-conditional packages for a
        a specified @group name.

        Arguments:
            group:  The name of the group to obtain the package list for

        Returns:
            A list of packages for the specified group

        """
        logger = self._class_logger.getChild("get_packages_for_group")
        _pretty_printer = pprint.PrettyPrinter()
        if self._group_packages is None:
            logger.debug("self._group_packages is None")
            self._group_packages = self.parse_comps_xml()
            logger.debug("self._group_packages is now {}".format(
                _pretty_printer.pformat(self._group_packages)))
        return self._group_packages[group]

    def get_primary_db(self):
        """
        Downloads the bz2 compressed repomd primary database in SQLite format,
        de-compresses it, writes it to a temporary file and returns the
        filename

        Returns:
            The temporary filename of the decompressed primary database

        """
        logger = self._class_logger.getChild(
            "get_primary_db")
        decompressor = bz2.BZ2Decompressor()
        logger.debug("Attempting to create temporary SQLite file")
        _temp_file_handle = NamedTemporaryFile(delete=False, suffix=".sqlite")
        logger.debug("Will write SQLite3 database to {}".format(
            _temp_file_handle.name))
        _temp_file_handle.write(
            decompressor.decompress(
                self._downloader.get_file("", self.find_primary_db())))
        logger.debug("Closing temporary file")
        _temp_file_handle.close()
        return _temp_file_handle.name

    def is_sqlite_3(self, filename):
        """
        Checks whether the specified filename is a valid SQLite3 file.

        Arguments:
            filename:  The name of the file to check

        Returns:
            True if the specified file smells like a SQLite3 database file,
            False otherwise.

        """
        logger = self._class_logger.getChild("is_sqlite_3")
        logger.debug("Checking filename {}".format(filename))
        if os.path.getsize(filename) < 100:
            # SQLite database file header is 100 bytes
            logger.error("{} has size < 100 bytes".format(filename))
            return False

        try:
            logger.debug("Attempting to open {}".format(filename))
            with open(filename, 'rb') as _file_handle:
                header = _file_handle.read(100)
                logger.debug("{} header is '{}'".format(filename, header))
        except IOError as _error:
            logger.error(
                "{}: IOError({}) {}".format(
                    filename, _error.errno, _error.strerror))
            return False

        return header[:16] == 'SQLite format 3\x00'

    def open_repo(self):
        """
        Creates a database connection to the SQLite3 database

        Returns:
            Nothing

        """
        logger = self._class_logger.getChild("open_repo")
        logger.debug("Opening connection to {}".format(self.filename))
        self._connection = sqlite3.connect(self.filename)

    def close_repo(self):
        """
        Closes the database connection to the SQLite3 database

        Returns:
            Nothing

        """
        logger = self._class_logger.getChild("close_repo")
        logger.debug("Closing connection to {}".format(self.filename))
        self._connection.close()

    def clean_up(self):
        """
        Responsible for cleaning up the resources when exiting the resource
        manager. Requests that the repo then removes the temporary file
        created when it was originally downloaded.

        Returns:
            Nothing

        """
        logger = self._class_logger.getChild("clean_up")
        logger.debug("Cleaning up - closing repo")
        self.close_repo()
        logger.debug("Cleaning up - removing file {}".format(self.filename))
        os.remove(self.filename)

    def execute_repo_query(self, query_string, params):
        """
        Queries the repo database using the parameters and query string
        provided

        Args:
            query_string (string): The SQL query to run on the database
            params (tuple): The parameters to inject in to the query string

        Returns:
            The list of results from the query

        """
        logger = self._class_logger.getChild("execute_repo_query")
        logger.debug(
            "Executing query {} with params {}".format(query_string, params))
        cursor = self._connection.cursor()
        cursor.execute(query_string, params)
        results = []
        for row in cursor.fetchall():
            results.append(row[0])
            logger.debug("Results of query are {}".format(results))
        return results

    def get_rpm_for_package(self, package):
        """
        For a specified package name, get the relative name of the RPM file
        which provides it.

        Args:
            package (string): The name of the package

        Returns:
            The list of RPM files providing the specified package

        """
        logger = self._class_logger.getChild("get_rpm_for_package")
        logger.debug("Querying RPM file for package {}".format(package))
        rpm_files = self.execute_repo_query(
            'select location_href as filename from packages where name=? \
            and (packages.arch="x86_64" or packages.arch="noarch")',
            (package,))
        logger.debug("Got back {} results".format(len(rpm_files)))
        assert len(rpm_files) > 0  # No result means RPM file can't be resolved
        assert len(rpm_files) < 2  # More than one result = unexpected error
        logger.debug("Result was {}".format(rpm_files))
        return rpm_files

    def get_requires_for_package(self, package):
        """
        For the package with the specified package name, get the list of
        dependencies required to be installed along with it.

        Args:
            package (string): The name of the package

        Returns:
            A list of unique dependencies for the package (sometimes
            dependencies are specified more than once for a given package).

        """
        logger = self._class_logger.getChild("get_requires_for_package")
        logger.debug("Querying requirements for package {}".format(package))
        requirements = unique(self.execute_repo_query(
            'select requires.name from requires inner join packages \
            on requires.pkgKey = packages.pkgKey \
            where packages.name=?', (package,)))
        logger.debug("Result was {}".format(requirements))
        return requirements

    def get_package_for_requirement(self, req):
        """
        For a given dependent requirement (which may be a package name or a
        file on the file system), obtain the list of packages which provide it.

        Args:
            req (string): The requirement to resolve

        Returns:
            A list of unique package names which provide the requirement

        """
        logger = self._class_logger.getChild("get_package_for_requirement")
        logger.debug("Querying for the package which meets {}".format(req))
        pkgs = unique(self.execute_repo_query(
            'select packages.name from provides inner join packages on \
                provides.pkgKey = packages.pkgKey \
                where provides.name=?', (("%s" % req),)) +
                      self.execute_repo_query(
                          'select packages.name from files inner join \
                          packages on files.pkgKey = packages.pkgKey where \
                          files.name=?', (req,)))
        logger.debug("Result was {}".format(pkgs))
        return pkgs

    def get_expected_hash_for_rpm(self, rpm_file):
        """
        For a specified RPM file, get the expected hash and function used to
        generate it from the repo database.

        Args:
            rpm_file (string): The name of the RPM file to get the hash for

        Returns:
            tuple containing the expected hash and the function used to
            generate it e.g. ( "aaabbbccc...", "sha256" )

        """
        logger = self._class_logger.getChild("get_expected_hash_for_rpm")
        logger.debug("Querying for hash for RPM {}".format(rpm_file))
        file_hash = self.execute_repo_query(
            'select pkgId from packages where location_href=?', (rpm_file,))
        logger.debug("Result was {}".format(file_hash))
        assert len(file_hash) == 1  # Query returned wrong number of pkgIds
        logger.debug("Querying for hash type for RPM {}".format(rpm_file))
        hash_type = self.execute_repo_query(
            'select checksum_type from packages where location_href=?',
            (rpm_file,))
        logger.debug("Result was {}".format(hash_type))
        assert len(hash_type) == 1  # Wrong number of checksum types
        return (file_hash[0], hash_type[0])

    def __enter__(self):
        """
        The mandated __enter__ function used with the "with foo as f:"
        construct

        Returns:
            self

        """
        logger = self._class_logger.getChild("__enter__")
        logger.debug("Entering resource manager for RepoParser")
        self.open_repo()
        return self

    def __exit__(self, _exc_type, _exc_value, _traceback):
        """
        Called when exiting the resource manager "with foo as f:" construct.
        Responsible for cleaning up the resource and in this case calling a
        function to close connections and delete temporary files etc.

        Returns:
            Nothing

        """
        logger = self._class_logger.getChild("__exit__")
        logger.debug("Exiting resource manager for RepoParser")
        self.clean_up()

    def __init__(self, rootLogger, repoURL, proxies):
        """
        The constructor for the RepoParser class.

        Args:
            repoURL (str): The base URL of the source yum repository
            proxies (dict): A dictionary containing the URLs of proxies to
            direct requests through.
            {
                http:   http proxy url
                https:  https proxy url
            }

        Returns:
            An instance of the RepoParser class.

        """
        self._class_logger = rootLogger.getChild(self.__class__.__name__)

        logger = self._class_logger.getChild("__init__")
        logger.debug("Initialising instance of RepoParser")

        logger.debug("proxies are {}".format(proxies))
        self._proxies = proxies

        self._repomd = None
        self._group_packages = None
        self._connection = None
        self._downloader = None

        # Do we need to get files from the Internet or from a locally
        # mounted dir?
        logger.debug("repoURL is {}".format(repoURL))
        if repoURL.startswith("file://"):
            self._base_url = os.path.realpath(
                os.path.abspath(repoURL[7:])) + "/"
            logger.info("Repo source is a directory: {}".format(
                self._base_url))
            self._downloader = FileSystemRetriever(rootLogger, {})
        else:
            self._base_url = repoURL + "/"
            logger.info("Repo source is a website: {}".format(self._base_url))
            self._downloader = HTTPDownloader(rootLogger, self._proxies)

        self._comps_xml = unicode(self.download_comps_xml(), errors='ignore')
        logger.debug("Length of comps XML is {}".format(len(self._comps_xml)))

        self.filename = self.get_primary_db()
        if not self.is_sqlite_3(self.filename):
            raise OSError(
                os.errno.EPERM,
                "This doesn't appear to be a valid SQLite3 file",
                self.filename)
