"""
FileGetter
"""

from __future__ import print_function

import sys
import os


class FileGetter(object):
    """
    Parent class for FileSystemRetriever and HTTPDownloader which, in this
    instance, contains only the output functions responsible for reporting on
    file copy/download progress.
    """

    def pretty_print(self, prefix, progress):
        """
        Outputs a line of text including a specified prefix and a row of hashes
        representing the percentage of completion of the task that called it.

        Args:
            prefix: The string to display at the start of the line
            progress: The percentage (as a float 0 <= progress <= 100)

        Returns:
            Nothing

        """
        _, columns = os.popen('stty size', 'r').read().split()
        terminal_width = int(columns)
        pcent_line = "{0:.2f}%".format(progress*100)
        hash_line = "#" * int(
            (terminal_width-len(pcent_line)-(len(prefix)+3)) * progress
        )
        line_new = prefix + " " + hash_line + " " + pcent_line.ljust(8) + '\r'
        print(line_new, end="")
        sys.stdout.flush()

    def terse_print(self, prefix, progress):
        """
        Outputs the supplied prefix and "Download complete" only when the
        progress is 100%. This is useful when output is being redirected to a
        file or a pipe to avoid lots of lines of hashes being output to the
        file.

        Args:
            prefix: The string to display at the start of the line
            progress: The percentage (as a float 0 <= progress <= 100)

        Returns:
            Nothing

        """
        if progress == 1:
            print("%s: Copy complete" % prefix, end="")
            sys.stdout.flush()
        # else do nothing

    def __init__(self, root_logger, proxies):
        self._class_logger = root_logger.getChild(self.__class__.__name__)
        self._proxies = proxies

        if sys.stdout.isatty():
            self._print_func = self.pretty_print
        else:
            self._print_func = self.terse_print
