"""
KSFileParser
"""

from pykickstart.parser import KickstartParser
from pykickstart.version import makeVersion


class KSFileParser(object):
    """
    Provides functionality to parse/query a Kick Start config file and obtain
    the requested packages, groups and exclusions.
    """

    def __init__(self, rootLogger, ks_file):
        """
        The constructor for the KSFileParser class.

        Args:
            ksFile (str): The path to the Kick Start file which will be parsed

        Returns:
            An instance of the KSFileParser class containing the list of
            packages, groups and exclusions specified in the Kick Start file.

        """
        self._class_logger = rootLogger.getChild(self.__class__.__name__)
        logger = self._class_logger.getChild("__init__")
        logger.debug(
            "Initialising instance of KSFileParser for file {}".format(
                ks_file))

        self._ks_file = ks_file

        logger.debug("Going to instantiate a KickstartParser")
        ksparser = KickstartParser(makeVersion(), errorsAreFatal=False,
                                   missingIncludeIsFatal=False)

        logger.debug("Going to read {}".format(self._ks_file))
        ksparser.readKickstart(self._ks_file)

        self.groups = []
        self.packages = []
        self.excluded_packages = []

        for group in ksparser.handler.packages.groupList:
            logger.debug("Found group {}".format(group))
            self.groups.append(group)

        for package in ksparser.handler.packages.packageList:
            logger.debug("Found package {}".format(package))
            self.packages.append(package)

        for package in ksparser.handler.packages.excludedList:
            logger.debug("Excluding package {}".format(package))
            self.excluded_packages.append(package)
