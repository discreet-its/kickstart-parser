"""
FileSystemRetriever
"""

from __future__ import print_function

import os

from FileGetter import FileGetter


class FileSystemRetriever(FileGetter):
    """
    Retrieves files from the file system, reporting progress to the user as it
    does so.
    """

    def get_file(self, prefix, source):
        """
        Retrieve a file from the file system and display the progress to the
        user.
        """
        logger = self._class_logger.getChild("get_file")

        data = ""

        logger.debug("Getting file {}".format(source))

        tot = os.path.getsize(source)
        logger.debug("File size is {}".format(tot))
        prog = 0.0
        blocksize = 65536

        try:
            with open(source, 'rb') as file_handle:
                buf = file_handle.read(blocksize)
                while len(buf) > 0:
                    data = data + buf
                    prog = prog + len(buf)
                    self._print_func("{} {}".format(
                        prefix, os.path.basename(source)),
                                     (prog/tot))
                    buf = file_handle.read(blocksize)
        except IOError as error:
            # Getting here meant we couldn't open the file for reading
            logger.error("{}: IOError({}) {}".format(file, error.errno,
                                                     error.strerror))
        print()
        return data
