#!/bin/env python
"""
This program parses a given Kickstart file, extracts requested packages and
groups, downloads the required files including all dependencies from a
specified 'yum' repo server and stores the downloaded files in a specified
directory.

usage: gather_packages.py [-h] -r REPO -k KICKSTART -o OUTPUTDIRECTORY
                          [-p PROXY]

required arguments:
  -r REPO             The source repository URI
  -k KICKSTART        Path to kickstart file to query
  -o OUTPUTDIRECTORY  Path to directory where downloaded packages will be
                      stored

optional arguments:
  -h, --help          show this help message and exit
  -p PROXY            Proxy URL



"""

from __future__ import print_function

import argparse
import os
import sys
import logging

from KSFileParser import KSFileParser
from RepoParser import RepoParser
from UtilFunctions import hash_check_file, unique
from HTTPDownloader import HTTPDownloader
from FileSystemRetriever import FileSystemRetriever


class PackageGatherer(object):
    """
    The top level class responsible for parsing the specified Kick Start file,
    querying the specified yum repo to work out what files to download for the
    requested packages, downloading them and all required dependencies.
    """

    def __init__(self, rootLogger):
        """
        The constructor and "main" code for the PackageGatherer class.
        No point doing any logging before the parameters which set the level
        are processed as nothing will be output
        """
        self._class_logger = rootLogger.getChild(self.__class__.__name__)
        logger = self._class_logger.getChild("__init__")

        # Create an instance of argparse.ArgumentParser to parse the command
        # line options passed in by the user.
        parser = argparse.ArgumentParser(
            description="This program parses a given Kickstart file, extracts \
            requested packages and groups, downloads the required files \
            including all dependencies from a specified 'yum' repo server and \
            stores the downloaded files in a specified directory.")

        # We want to indicate optional and expected arguments, but not mandate
        # the order they are given on the command line.
        optional_args = parser._action_groups.pop()
        required_args = parser.add_argument_group('required arguments')
        parser._action_groups.append(optional_args)

        # -r specifies the URL to the source repository
        optional_args.add_argument("-r",
                                   help="The source repository URI",
                                   dest="repo",
                                   required=True,)

        # -k specifies the path to the kickstart file to process
        required_args.add_argument("-k",
                                   help="Path to kickstart file to query",
                                   dest="kickstart",
                                   required=True,)

        # -o specifies the directory downloaded package files will be stored in
        required_args.add_argument("-o",
                                   help="Path to directory where downloaded \
                                       packages will be stored",
                                   dest="outputDirectory",
                                   required=True,)

        # -p (optional) specifies the proxy to use
        optional_args.add_argument("-p",
                                   help="Proxy URL",
                                   dest="proxy",)

        # -v (optional) increase verbosity
        optional_args.add_argument("-v",
                                   action="count",
                                   default=0,
                                   help="Increase verbosity. -v outputs info,\
                                   -vv will result in full debug\
                                   output - not for the faint hearted",
                                   dest="debugLevel",)

        self.args = parser.parse_args()

        rootLogger.setLevel(30-(self.args.debugLevel*10))

        print("""
        PackageGatherer - parse a Linux Kickstart file for requested packages
        and download them from a remote repo to the filesystem.
        """)
        print("Kickstart file to process: {}".format(self.args.kickstart))
        print("Repo to get files from: {}".format(self.args.repo))
        print("Output directory to store packages in: {}".format(
            self.args.outputDirectory))
        print("Proxy to download files through: {}".format(
            self.args.proxy or "None"))
        print()
        sys.stdout.flush()

        logger.info("Using repo {}".format(self.args.repo))
        logger.info("Using proxy {}".format(self.args.proxy))
        logger.info("Output directory is {}".format(self.args.outputDirectory))

        # Check output directory exists and is writable
        if not os.path.exists(self.args.outputDirectory):
            error_and_exit("Output directory does not exist")

        if not os.path.isdir(self.args.outputDirectory):
            error_and_exit("Output location is not a directory")

        if not os.access(self.args.outputDirectory, os.W_OK):
            error_and_exit("Unable to write to output directory")

        proxies = {}
        if self.args.proxy is not None:
            proxies = {
                'http': self.args.proxy,
                'https': self.args.proxy
            }

        print("Downloading repo data and groups files...")
        logger.debug("Going to instantiate a RepoParser")
        with RepoParser(rootLogger, self.args.repo, proxies) as parser:
            files_to_copy = []
            resolved_packages = []
            packages_to_resolve = []

            logger.debug("Going to instantiate a KSFileParser for {}".format(
                self.args.kickstart))

            print()
            print("Parsing Kickstart file...")
            ksparser = KSFileParser(rootLogger, self.args.kickstart)

            # Resolve the requested groups and add those packages too.
            logger.info("{} groups were requested - parsing them".format(
                len(ksparser.groups)))
            for group in ksparser.groups:
                logger.info("\tProcessing group {}".format(group))
                packages_to_resolve = packages_to_resolve + \
                    parser.get_packages_for_group("{}".format(group))

            packages_to_resolve += ksparser.packages
            logger.info("{} additional packages were requested".format(
                len(ksparser.packages)))
            logger.debug("Additional packages requested are: {}".format(
                packages_to_resolve))

            # Spin will not install unless grub2 packages are included
            logger.info("Adding grub2")
            packages_to_resolve += ['grub2']

            # Remove and duplicates - some groups contain the same package
            packages_to_resolve = unique(packages_to_resolve)

            logger.info("{} exclusions were found".format(
                len(ksparser.excluded_packages)))
            for package in ksparser.excluded_packages:
                if package in packages_to_resolve:
                    logger.info("\tExcluding package {}".format(package))
                    packages_to_resolve.remove(package)

            logger.debug("Final list of packages: {}".format(
                packages_to_resolve))

            print("Resolving dependencies...")
            # Loop for each package to resolve, calculating its dependent
            # packages and adding these to the resolve list
            while packages_to_resolve:
                num_remaining = len(packages_to_resolve)
                if num_remaining % 50 == 0:
                    logger.info("{} left to resolve...".format(
                        len(packages_to_resolve)))

                # Get the package from the top of the list
                pkg = packages_to_resolve.pop()
                logger.debug("Processing package {}".format(pkg))

                # Work out what RPM files provide this package and add it to
                # the list of files to copy
                rpm = parser.get_rpm_for_package(pkg)
                files_to_copy.append(rpm[0])
                resolved_packages.append(pkg)

                # Find out the deps for this pkg
                logger.debug(
                    "Calculating requirements for package {}".format(pkg))
                for requirement in parser.get_requires_for_package(pkg):
                    logger.debug(
                        "Package {} has dependent {}".format(pkg, requirement))
                    for dependent_pkg in parser.get_package_for_requirement(
                            requirement):
                        logger.debug(
                            "Requirement {} is met by package {}".format(
                                requirement, dependent_pkg))
                        if dependent_pkg not in packages_to_resolve and \
                                dependent_pkg not in resolved_packages:
                            logger.debug(
                                "{} has not yet been resolved, \
                                adding to list".format(dependent_pkg))
                            packages_to_resolve.append(dependent_pkg)
                        else:
                            logger.debug(
                                "{} is already resolved or already on the \
                                list to be resolved".format(dependent_pkg))

            total_files = len(files_to_copy)
            print("Downloading {} files...".format(total_files))
            print()

            if self.args.repo.startswith("file://"):
                self._base_url = os.path.realpath(
                    os.path.abspath(self.args.repo[7:]))
                logger.info("Repo source is a directory: {}".format(
                    self._base_url))
                self._downloader = FileSystemRetriever(rootLogger, {})
            else:
                self._base_url = self.args.repo
                logger.info("Repo source is a website: {}".format(
                    self._base_url))
                self._downloader = HTTPDownloader(rootLogger, proxies)

            count = 0
            for file_name in files_to_copy:
                count += 1

                output_file = self.args.outputDirectory + "/" + \
                    os.path.basename(file_name)

                # Get the expected hash for the file
                (expected_hash, hash_type) = \
                    parser.get_expected_hash_for_rpm(file_name)

                # If expected output already exists, checksum it
                if os.path.exists(output_file):
                    logger.debug("{} already exists, check-summing it".format(
                        output_file))
                    if hash_check_file(output_file, expected_hash,
                                       hash_type, logger):
                        print("( {:>4} / {:>4} ) {}".format(
                            count, total_files,
                            os.path.basename(output_file)) +
                              "- Skipping: already downloaded and hash " +
                              "check passed")
                    logger.debug("Hash OK")
                    continue

                logger.debug("Opening {} for writing".format(output_file))
                try:
                    with open(output_file, 'wb') as file_handle:
                        file_handle.write(self._downloader.get_file(
                            "( {:>4} / {:>4} )".format(count, total_files),
                            self._base_url + "/" + file_name))
                        file_handle.close()
                except IOError as _error:
                    logger.critical("{}: IOError({}) {}".format(
                        file_name, _error.errno, _error.strerror))
                    error_and_exit(
                        "Error while attempting to write {}: {}".format(
                            file_name, _error.strerror))

                # Hash the downloaded file
                if hash_check_file(output_file, expected_hash,
                                   hash_type, logger):
                    logger.debug("Hash OK")
                else:
                    # Hash check on file failed
                    os.remove(output_file)
                    error_and_exit(
                        "Checksum error on downloaded file - aborting")


def error_and_exit(msg):
    """
    Handle an error situtation by informing user and exiting the programme

    Args:
        msg (str): The message to output

    """
    print("ERROR: {}".format(msg), end="\n\n")
    sys.exit(1)


if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    MAIN_LOGGER = logging.getLogger(__name__)
    PackageGatherer(MAIN_LOGGER)
