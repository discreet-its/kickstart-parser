"""
Utility functions
"""

from __future__ import print_function

import hashlib


def hash_check_file(file_to_check, expected_hash, hash_type, logger):
    """
    Check the hash of the specified file matches the expected hash of generated
    using the specified hash function.

    Args:
        file (str): The path to the file which will be checked
        expected_hash (str): The hash to check against(object)
        hash_type (str): The name of the hashlib function used to check the
            hash with.

    Returns:
        True if the hashes match, False otherwise.

    """
    hash_func = getattr(hashlib, hash_type)()

    blocksize = 65536
    try:
        with open(file_to_check, 'rb') as file_handle:
            buf = file_handle.read(blocksize)
            while len(buf) > 0:
                hash_func.update(buf)
                buf = file_handle.read(blocksize)
            actual_hash = hash_func.hexdigest()
            logger.debug("%s: Expected %s hash: %s", file_to_check,
                         hash_type, expected_hash)
            logger.debug("%s:   Actual %s hash: %s", file_to_check,
                         hash_type, actual_hash)
        return expected_hash == actual_hash
    except IOError as _error:
        # Getting here meant we couldn't open the file for reading
        logger.error("%s: IOError(%d) %s", file_to_check,
                     _error.errno, _error.strerror)
        return False


def unique(input_list):
    """
    Returns the supplied list with all duplicates stripped out.

    Args:
        a (list): The list to process

    Returns:
        the new list consisting of all the unique elements of a.

    """
    return list(set(input_list))
